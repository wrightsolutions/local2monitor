#
python -c "import os; \
import jinja2 as j2; \
j2env=j2.Environment(loader=j2.FileSystemLoader('/tmp')); \
dict1 = {'hostname': 'server1',
'host4': '10.10.1.11',
'hostalias': '',
'checkinterval_disk': '1',
'checkinterval_portcheck': '1',
'congroup_disk': 'supportlist',
'congroup_portcheck': 'supportlist',
'flapdetect_disk': '0',
'flapdetect_portcheck': '0',
'maxattempts_disk': '4',
'maxattempts_portcheck': '4',
'notinterval_disk': '15',
'notinterval_portcheck': '10',
'retryinterval_disk': '1',
'retryinterval_portcheck': '1'
}; \
print j2env.get_template('hosts_services.j2').render(dict1)"
RC_=$?
exit $RC_
