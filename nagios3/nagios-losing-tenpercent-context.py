#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import re
import fileinput
from datetime import datetime as dt

re_lostNpercent = re.compile('\slost [1-9]')
#re_keepalive = re.compile("\d")
#re_keepalive = re.compile("404")
#re_keepalive = re.compile('"\s[0-9]')

stored_line = None

def isodated(nixtime):
	#print nixtime
	return dt.isoformat(dt.fromtimestamp(nixtime))	

def str_isodated(str_given):
	nixtime = int(str_given[1:11])
	str_ret = "[{0}{1}".format(isodated(nixtime),
		str_given[11:])
	return str_ret

def print_line_plus_stored(match_lastindex):
	global stored_line
	pos = int(match_lastindex)
	if stored_line is not None:
		print stored_line
		stored_line = None

	print "{0} LOST {1}".format(str_isodated(line[:pos-7]),
		line[pos-1:]).strip()
	return

""" Variables stored_line and print_also_count are used so that some
context is given to a matching line. One line previous and Two lines following. """

line_count = 0
print_also_count = 0
for line in fileinput.input():
	line_count += 1
	#print "{0} ({1}): {2} {3}".format(line_count,len(line),line[0:12],line[40:].strip())
	m_lostNpercent = re_lostNpercent.search(line)
	print_line_flag = True
	if (m_lostNpercent is None):
		print_line_flag = False
		"""
		debug_line = 'setting to False block 1'
		if len(line) > 12:
			debug_line = '{0} for line beginning {1}'.format(debug_line,line[:12])
		print debug_line
		"""
	else:
		""" print m_lostNpercent.group(0) """
		""" if (len(m_lostNpercent.groups()) < 1): """
		if (len(m_lostNpercent.group(0)) < 1):
			""" print 'setting to False block 2' """
			print_line_flag = False

	""" An easier alternative to the print_line_flag is probably
	only to test line when len(line) is positive """

	#print print_line_flag
	stored_line = None
	if print_line_flag:
		""" print "{0}-{1}".format(m_lostNpercent.start(),m_lostNpercent.end()) """
		print_line_plus_stored(m_lostNpercent.end())
		print_also_count = 2
	else:
		if print_also_count > 0:
			print str_isodated(line).strip()
			print_also_count -= 1
		else:
			stored_line = str_isodated(line)
