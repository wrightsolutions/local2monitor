#!/bin/bash
OUTF_=/tmp/hundred.txt
printf -- '-- MARK --\n%.0s' \"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30\" > ${OUTF_}
printf '<signal \n%.0s' \"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20\" >> ${OUTF_}
printf '/lib/x86_64-\n%.0s' \"1 2 3 4 5 6 7 8 9 10\" >> ${OUTF_}
printf '/usr/bin/duo(duo_string_new_size+0x4c) [0x56677c]\n%.0s' \"1 2 3 4 5 6 7 8 9 10\" >> ${OUTF_}
printf 'InternalAllocateStr\n%.0s' \"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30\" >> ${OUTF_}
OUTF2_=/tmp/sixty.txt
printf -- '-- unable to restart --\n%.0s' \"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30\" > ${OUTF2_}
printf -- '-- still unable to restart --\n%.0s' \"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30\" >> ${OUTF2_}
cat ${OUTF_} ${OUTF2_} > /tmp/both.txt
