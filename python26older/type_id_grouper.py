#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Analyze the first 500 lines of content from stdin and output PER ID2 (till id) a summary count
of the different message types
Takes 2 arguments:
 First arg is number of lines of content from stdin to process (default 70000)
 Second arg is the number of the id field on which to group (default 2 which is often till number)

If you prefer line breaks then replace the comma with newline by postprocessing
python ./type_id_grouper.py 500 2 | tr ',' '\n' | sed  's$</$\n</$'

"""


import fileinput
import re
from os import path,sep
from sys import argv,stdin
from string import printable
prog = argv[0].strip()
prog_basename = path.basename(prog)
prog_basename_extensionless = path.splitext(prog_basename)[0]

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3
STATE_DEPENDENT=4
STATE_UNKNOWN_MESSAGE='STATE UNKNOWN'
DESCRIPTION='Check if n lines of tail of log contains >= c occurences of target string'

RE_LIB_X86_64 = re.compile('/lib/x86_64-')
RE_STACK_LT_SIGNAL = re.compile('<signal ')
#RE_STACK_CROTCHETS = re.compile('\(.+0x.+\)\s+\[0x\w*\d+\w*\]\Z')
#RE_STACK_CROTCHETS = re.compile('\s+\[0x\w*\d+\w*\]\Z')
RE_STACK_CROTCHETS = re.compile('\(.+0x.+\)\s+\[0x\w*\d+\w*\]\Z')
RE_STACK_CROTCHETS_SLASHED = re.compile('/.*\(.+0x.+\)\s+\[0x\w*\d+\w*\]\Z')
RE_INTERNAL_ALLOCATE_STR = re.compile('InternalAllocateStr')
RE_CROTCHETED_LETTER = re.compile('-\s+\[\w\]\s+-')
RE_TYPE_ID = re.compile('type:\s+\w*\s+id:')
RE_CROTCHETED_LETTER_TYPE_ID = re.compile('-\s+\[\w\]\s+\-.*type:\s+\w*\s+id:')
RE_TYPE = re.compile('type:\s+')
RE_ID = re.compile('id:\s+')

SET_PRINTABLE = set(printable)

list_of_lists = []
dict_of_lines = {}
LINELIMIT = 70000
NUM_OF_INTEGERS = 16


def line_formatted(id_string,dict_of_counts):
        """ chr(44) is comma """
        sep = ''
        line = '<' + id_string + '>'
        for linetype in sorted(dict_of_counts.iterkeys()):
                line = line + sep + linetype + ':' + str(dict_of_counts[linetype])
                sep = chr(44)
        line = line + '</' + id_string + '>'
        return line


def process_fileinput_input(fileinput_given,linelimit=500,id_field_num=2):
	""" Populate the list named lines from fileinput and then
        chr(58) is colon
	"""

	""" fileinput_given.filelineno() is the file number in the
	current file and fileinput_given.lineno() is the cummulative
	line number
	"""

        dict_of_dicts = {}
	lines = []
	for line in fileinput_given.input(argv[4:]):
                #if RE_TYPE_ID.search(line):
                if RE_CROTCHETED_LETTER_TYPE_ID.search(line):
                        line_prefix,line_body = RE_TYPE.split(line)
                        type_plus = line_body.strip()
                        linetype,id1plus = RE_ID.split(type_plus)
                        linetype = linetype.strip()
                        id_array = id1plus.split(chr(58))
                        if len(id_array) != 3:
                                continue
                        id1 = id_array[0]
                        id2 = id_array[1]
                        id3 = id_array[2]
                        if id2 in dict_of_dicts:
                                type_dict = dict_of_dicts[id2]
                        else:
                                type_dict = {}
                        if linetype in type_dict:
                                type_dict[linetype] = 1+type_dict[linetype]
                        else:
                                type_dict[linetype] = 1
                        dict_of_dicts[id2] = type_dict
                        print linetype,id1,id2,id3,id1plus,type_plus,line_prefix

	return dict_of_dicts


if __name__ == "__main__":

	if len(argv) < 2:
		exit(130)

	target = None
        id_field_num_str = None
        id_field_num = 2
	if len(argv) > 0:
		linelimit_string = argv[1]
		linelimit = int(linelimit_string)
                if len(argv) > 1:
                        id_field_num_str = argv[2]

        if id_field_num_str is None:
                pass
                #print STATE_UNKNOWN_MESSAGE
                #exit(STATE_UNKNOWN)
        else:
                try:
                        id_field_num = int(id_field_num_str)
                except Exception:
                #except Exception, e:
                        print STATE_UNKNOWN_MESSAGE
                        #exit(STATE_UNKNOWN)

	exit_rc = 0
	processed_count = 0

	if id_field_num > 0:
		dict_of_dicts = process_fileinput_input(fileinput,linelimit,id_field_num)
                for did in sorted(dict_of_dicts.iterkeys()):
                        dict_of_counts = dict_of_dicts[did]
                        print line_formatted(did,dict_of_counts)

        print len(dict_of_dicts)

        """
        count7matching = tuple_of_integers[7]
        if count7matching >= threshold_warning_int:
                print "WARNING: %d occurences in text tail of %s" % (count7matching,target)
                exit(STATE_WARNING)
        elif count12 > 0:
                print "CRITICAL: tail -n %d of log/text indicates stack trace!" % linelimit
                exit(STATE_CRITICAL)
        else:
                print "OK: text tail did not contain target"
                #print("OK: text tail did not contain target".format(target))
                exit(STATE_OK)
        """

        # May seem counter-intuitive to exit WARNING when there is possible CRITICAL,
        # however the primary purpose of this script (hinted in name) is to give
        # WARNING when encountering target
                
	exit(exit_rc)

