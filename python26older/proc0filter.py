#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015, Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

""" Process output from ps and filter on given pattern.
Write matches to an sqlite db.
Output a single summary line with a list of pcpus

Designed for older servers before recent python-psutil available in Debian
so we parse ps output instead of utilising the python-psutil helper.
"""

from __future__ import print_function

from collections import namedtuple
from datetime import datetime as dt
from os import path,sep
from sys import argv
import re
import shlex
import subprocess

try:
    import sqlite3 as db3
except ImportError:
    print("Is sqlite (sqlite3) installed?")
    #print("Is sqlite (sqlite3) installed? Import error {0}".format(e))
    """ apt-get install sqlite3
    # http://archive.debian.net/lenny/sqlite3
    """
    exit(191)

BUFFER_SIZE = 0

prog = argv[0].strip()
prog_basename = path.basename(prog)
prog_basename_extensionless = path.splitext(prog_basename)[0]

ProcessInfo = namedtuple('ProcessInfo', ['pcpu','pid','cputime','mrss','msize','mvsize','started',
                                         'user','status','args','before','after','line','notes'])


def sorted_floats(list_original,reverse=False):
    list_floats = [ float(item) for item in list_original ]
    return sorted(list_floats,reverse=reverse)


def process_lines(stdout_unsplit,pattern,start_field=0,double_zero=False,zombies=False):
    """ Default is to discard lines that are double_zero so have 0 rss and 0 size
    however if zombies is set True then that overrides the above behaviour
    ProcessInfo = namedtuple('ProcessInfo', 
    ['pcpu','pid','cputime','mrss','msize','mvsize','started','user',
    'status','args','before','after','line','notes'])
    """
    nt_matching = []
    for idx,line in enumerate(stdout_unsplit.splitlines()):

        line_array = line.split()
        pcpu = line_array[0]
        try:
            rss = line_array[3]
            mrss = int(rss)
        except:
            err_str = "Error processing ps output line {0}".format(idx)
            err_str = "{0} having pcpu={1}".format(err_str,pcpu)
            err_str = "{0} and line={1}".format(err_str,line)
            raise RuntimeError(err_str)
        try:
            size = line_array[4]
            msize = int(size)
            vsize = line_array[5]
            mvsize = int(vsize)
        except:
            err_str = "Error processing ps output line {0}".format(idx)
            err_str = "{0} having pcpu={1}".format(err_str,pcpu)
            err_str = "{0} and line={1}".format(err_str,line)
            raise RuntimeError(err_str)
        """ size is total program size (pages)
        size is text+data+stack
        """

        status = line_array[8]
        #print(line,mrss,msize)
        if double_zero is not True and 0 == sum([mrss,msize]):
            """ We have been instructed to discard lines
            where rss and size are both zero. We will use 'continue'
            to exit this iteration of the loop
            """
            if zombies is True and 'Z' in status:
                pass
            else:
                continue

        if prog_basename in line:
            # Exclude ourself (this program) from possibility of matching
            continue

        started = line_array[6]
        owner = line_array[7]

        matches0 = re.match("(.*)({0}\s+{1})(.*)".format(started,owner),line)
        if matches0:
            before = matches0.group(0)
            started_owner = matches0.group(1)
            after = matches0.group(2)
        else:
            raise RuntimeError("Error during line splitting using started+owner")

        pattern_grouped = "(.*)({0})(.*)".format(pattern)
        #print(pattern_grouped)
        matches = re.search(pattern_grouped,line)
        if matches:
            before = matches.group(0)
            pattern_matched = matches.group(2)
            after = matches.group(3)
            notes = "[{0}]{1}".format(pattern_matched,after)
        else:
            continue

        pid = line_array[1]
        time = line_array[2]
        cmdline = ' '.join(line_array[9:])
        nt_matching.append(ProcessInfo(pcpu,pid,time,mrss,msize,mvsize,started,owner,
                                       status,cmdline,before,after,line,notes))

    return nt_matching


def process_filter(pattern,start_field):

    process_out = None
    lines = []

    pscmd_as_list = ['/bin/ps','-eo','pcpu,pid,time,rss,size,vsize,start_time,user,stat,args','--no-headers']
    #print(pscmd)
    """
    pscmd = "/bin/ps -eo pcpu,pid,time,rss,size,vsize,start_time,user,stat,args --no-headers | {0}".format(sortcmd)
    psproc = subprocess.Popen(shlex.split(pscmd),bufsize=BUFFER_SIZE,
                              stdout=subprocess.PIPE)
    """
    psproc = subprocess.Popen(pscmd_as_list,bufsize=BUFFER_SIZE,
                              stdout=subprocess.PIPE)
    sortcmd = "/usr/bin/sort -b -nk1 -k5 -r"
    psort = subprocess.Popen(shlex.split(sortcmd),bufsize=BUFFER_SIZE,
                             stdin=psproc.stdout,stdout=subprocess.PIPE)

    """ psproc.communicate()
    When chaining pipes of popen we only call the final .communicate() rather than
    each point in the chain. If you issue more than one .communicate() then your
    results may be unpredictable
    """
    psortout, psorterr = psort.communicate()
    psproc.stdout.close()  # Allow pdirs to receive a SIGPIPE if pgrep exits.
    psort.stdout.close()  # Allow pgrep to receive a SIGPIPE if psummer exits.
    psort_rc = psort.returncode

    if 0 == psort_rc and psortout:
        process_out = psortout
    else:
        print(psort_rc,psortout)
        print(psorterr)
        raise RuntimeError("""
        Error during ps command call .""".format(pscmd)
        )

    nt_matching = process_lines(process_out,pattern,start_field,False,False)

    pcpu_list = []

    for procinfo in nt_matching:
        #print(procinfo.line)
        pcpu_list.append(procinfo.pcpu)
        lines.append(procinfo.line)

    if 0 == len(pcpu_list):
        print("No pcpu list to report as no lines matching pattern={0}".format(pattern))
    else:
        pcpu_list_sorted = sorted_floats(pcpu_list,reverse=True)
        list_sorted = [ str(item) for item in pcpu_list_sorted ]
        line1 = "{0} for pattern={1}".format(' '.join(list_sorted),pattern)
        print(line1)
            
    return nt_matching



class SqliteDb(dict):
    """ SqliteDb object which initially holds the path to sqlite
    formed from a series of fields
    Later this could be developed further by replacing use of
    globals such as dbcon.
    With future use in mind we extend dictionary tables_dict with name, status
    has a skeleton implementation
    """

    NAME_FROM_SQLITE_MASTER_TEMPLATE = "SELECT name FROM sqlite_master WHERE type='table' AND name='{0}';"
    PROCPERCENT_TABLENAME = 'procpercent'
    SQLSTRING_CREATE_PROC = ("CREATE TABLE IF NOT EXISTS"
                             " {0}(pid integer NOT NULL,"
                             " cmdword text NOT NULL, "
                             " sampled_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                             " pcpu real NOT NULL,"
                             " cputime real NOT NULL,"
                             " started text NOT NULL,"
                             " mrss real NOT NULL,"
                             " msize real NOT NULL,"
                             " mvsize real NOT NULL,"
                             " user text NOT NULL,"
                             " status text NOT NULL,"
                             " cmdline text NOT NULL,"
                             " notes text,"
                             " PRIMARY KEY(pid,cmdword,sampled_at,pcpu,cputime)"
                             " );".format(PROCPERCENT_TABLENAME))
    ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'
    #MEG_AS_FLOAT = float(2**20)
    MEG_AS_FLOAT = float(2**10)


    def __init__(self, pattern='/bin/', pathstem='/tmp/', pathsep=None):
        super(SqliteDb, self).__init__
        self._pattern = pattern
        self._pathstem = pathstem
        if pathsep is None:
            self._pathsep = sep
        else:
            self._pathsep = pathsep
        self._pid = None
        self._date_today_init = dt.today()
        # Note that date_today_init is fixed at class init time and never updated
        self._yyyyww = "{0}{1:<2}".format(self._date_today_init.year,
                                          self._date_today_init.isocalendar()[1])
        # Similarly yyyyww is based on date_today_init and so again is fixed in time
        self._dbfilepath = None
        if self._pattern is not None:
            patred = re.sub(r'\W+', '', self._pattern)
            """ Above we have removed non-word characters from pattern and assigned result to
            pattern reduced (patred)
            """
            if len(patred) > 1:
                self._dbfilepath = "{0}{1:<1}proc{2}__{3}cpu.sqlite".format(
                    self._pathstem,self._pathsep,patred,self._yyyyww)
        self._connection = None
        self._cursor = None


    """ pathstem property requires no setter as only set directly during class init. """
    @property
    def pathstem(self):
        return self._pathstem

    def pathsep_get(self):
        return self._pathsep

    def pathsep_set(self,pathsep):
        self._pathsep = pathsep
        
    pathsep = property(pathsep_get,pathsep_set)

    def pid_get(self):
        return self._pid

    def pid_set(self,pid):
        self._pid = pid
        
    pid = property(pid_get,pid_set)

    """ date_today_init property requires no setter as only set directly during class init. """
    @property
    def date_today_init(self):
        return self._date_today_init

    """ yyyyww property requires no setter as only set directly during class init. """
    @property
    def yyyyww(self):
        return self._yyyyww

    """ tables_dict property requires no setter """
    @property
    def tables_dict(self):
        return self

    """ tables_dict property requires no setter  """
    @property
    def tables_list(self):
        return self.keys()

    """ dbfilepath requires no setter as we do not want to expose it as publicly settable. """
    @property
    def dbfilepath(self):
        return self._dbfilepath

    """ connection property requires no setter """
    @property
    def connection(self):
        return self._connection

    def connection_commit(self,cursor_drop=True,catch=True):
        """ Here it is the cursor to which we issue a commit, but name chosen as often
        we are coupled with functionality of connection_close() in terms of logical calls
        This default to cursor_drop=True and so if that is not what you want then you
        should set False or be using another vehicle for issuing your 'commit;'
        """
        if self.cursor is not None:
            try:
                self.cursor.execute("commit;")
                if cursor_drop is True:
                    self.cursor_close()
            except:
                if catch is not True:
                    raise
        return

    def connection_close(self):
        if self.connection:
            self.connection.close()
            self._connection = None
        return self.connection

    """ cursor property requires no setter as we do not want to expose it as publicly settable """
    @property
    def cursor(self):
        """ Access the underlying cursor field but do not attempt any on demand creation """
        return self._cursor

    def cursor_close(self):
        if self.cursor:
            self.cursor.close()
            self._cursor = None
        return


    def cur(self):
        """ Not to be confused with the property 'cursor', this method will create a cursor
        on demand if one does not exist.
        """

        if self.cursor:
            pass
        else:
            try:
                if self.connection:
                    cur = self._connection.cursor()
                    self._cursor = cur
            except:
                pass

        return self.cursor


    def cursor_execute(self,sql_statement,rows_expected=0,debug=False):

        executed_flag = False

        if self.cursor is None:
            return executed_flag

        try:
            self.cur()
            if debug is True:
                print("Should have cursor and now will execute sql statement shown next.")
                print(sql_statement)
            self.cursor.execute(sql_statement)
            executed_flag = True
        except db3.ProgrammingError:
            #err_str = "Sqlite execute() ProgrammingError occurred: {0}".format(e)
            err_str = "Sqlite execute() ProgrammingError occurred: {0}".format(e)
            if debug: print(err_str)
            self.cursor_close()
            raise RuntimeError(err_str)
        except db3.Error:
            """
            err_str = "Sqlite execute() error {0}: {1}".format(
                database_path, e.args[0])
            """
            err_str = "Sqlite execute() error for db at {0}".format(self.dbfilepath)
            if debug: print(err_str)
            self.cursor_close()
            raise RuntimeError(err_str)
        except:
            err_str = "Sqlite error during .execute() create if not exists."
            if debug: print(err_str)
            self.cursor_close()
            raise RuntimeError(err_str)

        if 0 == rows_expected:
            rowcount = 0
        elif 1 == rows_expected:
            rows = self.cursor.fetchone()
            rowcount = rows[0]
        else:
            rows = self.cursor.fetchall()
            rowcount = rows[0]

        if executed_flag:
            if rowcount == rows_expected:
                pass
            else:
                executed_flag = False

        return executed_flag


    def create_table_procpercent(self,debug=False):
        """ Having a cursor object does not guarantee that the file was an sqlite
        database. A plain text file get a cursor, but fail on cur.execute() """

        if debug:
            print(SqliteDb.SQLSTRING_CREATE_PROC)

        self.cur()
        created_flag = self.cursor_execute(SqliteDb.SQLSTRING_CREATE_PROC,0,debug)

        return created_flag


    def connection_cursor(self,debug=False):
        if self.dbfilepath is None:
            return None

        ready_marker = 2

        if self.connection is None:

            ready_marker = 0

            if path.exists(self.dbfilepath):
                # Sqlite file exists but we have no connection?
                pass

            try:
                dbcon = db3.connect(self.dbfilepath,timeout=2)
                ready_marker += 1
            except db3.Error:
                """
                except db3.Error as e:
                print("Sqlite connection error occurred: {0}".format(e.args[0]))
                """
                err_str = "Sqlite connection Error occurred"
                raise RuntimeError(err_str)
            except Exception:
                err_str = "Sqlite connection Exception occurred"
                raise RuntimeError(err_str)

            if dbcon:
                dbcon.isolation_level = None
                self._connection = dbcon

                created_flag = self.create_table_procpercent(debug)
                if created_flag is True:
                    self[SqliteDb.PROCPERCENT_TABLENAME] = 'created'
                    ready_marker += 1
                else:
                    self.connection_close()

        if ready_marker > 1:
            """ Either we just did a connnect and table create or else we
            already had a connection in first place
            """
            try:
                #cur = self._connection.cursor()
                self.cur()
            except:
                # self._cursor = None
                self.cursor_close()

        return self.cursor


    def table_exists(self,table_name,dbquery=False):
        """ The table is deemed not to exist if there is no dictionary entry
        for the table or the dictionary can be equated to 0,None,False
        """
        if table_name not in self:
            return False

        confidence_level = 0

        try:
            if self[table_name]:
                confidence_level += 1
        except KeyError:
            # table not found in dictionary
            pass
        except:
            pass

        if dbquery is True:
            # Make more of an effort and do not just trust
            sql_query = SqliteDb.NAME_FROM_SQLITE_MASTER_TEMPLATE.format(table_name)
            self.cur()
            executed_flag = self.cursor_execute(sql_query)
            if executed_flag is True:
                confidence_level += 1

        if dbquery and confidence_level > 1:
            exists_flag = True
        elif confidence_level > 0:
            exists_flag = True
        else:
            exists_flag = False

        return exists_flag


    def table_exists_robust(self,table_name):
        return table_exists(self,table_name,True)
        

    def insert_tuple(self,sample_tuple,debug=False):

        inserted_flag = False
        #print("Next we insert into the procpercent table.")
        try:
            self.cur()
            self.cursor.execute("""INSERT INTO procpercent (pid,cmdword,sampled_at,pcpu,cputime,
started,mrss,msize,mvsize,user,status,cmdline,notes)
VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);
""",sample_tuple)
            inserted_flag = True
        except db3.OperationalError:
            #err_str = 'Sqlite execute() OperationalError occurred: {0}'.format(e)
            err_str = 'Sqlite execute() OperationalError occurred.'
            self.cursor_close()
            if debug: print(err_str)
        except db3.ProgrammingError:
            #err_str = 'Sqlite execute() ProgrammingError occurred: {0}.format(e)'
            err_str = 'Sqlite execute() ProgrammingError occurred.'
            self.cursor_close()
            if debug: print(err_str)
        except db3.Error:
            #err_str = 'Sqlite execute() error occurred: {0}'.format(e)
            err_str = 'Sqlite execute() error occurred.'
            self.cursor_close()
            if debug: print(err_str)
        except ValueError as e:
            #err_str = 'Sqlite insert failed ValueError: {0}'.format(e)
            err_str = 'Sqlite insert failed ValueError.'
            self.cursor_close()
            if debug: print(err_str)
        except Exception as e:
            #err_str = 'Sqlite insert failed: {0}'.format(e)
            err_str = 'Sqlite insert failed.'
            self.cursor_close()
            if debug: print(err_str)

        return inserted_flag


    def insert_named_tuple(self,named_tuple,debug=False):

        """
        #ProcessInfo = namedtuple('ProcessInfo', ['pcpu','pid','cputime','mrss','msize','mvsize','started',
        #                                         'user','status','args','before','after','line'])
        """
        nt = named_tuple

        mrss = nt.mrss / SqliteDb.MEG_AS_FLOAT
        msize = nt.msize / SqliteDb.MEG_AS_FLOAT
        mvsize = nt.mvsize / SqliteDb.MEG_AS_FLOAT

        cmdline = nt.args
        cmdword = cmdline.split()[0]
        #isotimeutc_created = dt.isoformat(dt.utcfromtimestamp(nixtime_created))
        sampled_at = dt.now().isoformat()

        tup = (nt.pid,cmdword,sampled_at,nt.pcpu,nt.cputime,nt.started,mrss,msize,mvsize,nt.user,
               nt.status,cmdline,nt.notes)

        return self.insert_tuple(tup,debug)


if __name__ == '__main__':
    exit_rc = 0

    pattern_given = '/sbin/'

    DEBUG = False

    if len(argv) > 1:
        pattern_given = argv[1]

    lines = []
    try:
        nt_matching = process_filter(pattern_given,0)
    except:
        exit_rc = 130
        raise

    if 0 == exit_rc and len(nt_matching) < 1:
        exit_rc = 140

    db = SqliteDb(pattern=pattern_given)
    cur = db.connection_cursor(DEBUG)
    #print("sqlite db at path {0} gives cur={1}".format(db.dbfilepath,cur))

    for nt in nt_matching:
        db.insert_named_tuple(nt,DEBUG)

    try:
        db.connection_commit()
        #print("Closing sqlite db at path {0}".format(db.dbfilepath))
        db.connection_close()
    except:
        pass

    exit(exit_rc)

