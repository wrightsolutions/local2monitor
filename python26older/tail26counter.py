#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015, Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

""" Analyze the first 500 lines of content from stdin and return some
summary counts.
In particular the location of the last (and first) entries that match
the input string are given close attention

printf -- '-- MARK --\n%.0s' {1..30} > /tmp/hundred.txt
printf '<signal \n%.0s' {1..20} >> /tmp/hundred.txt
printf '/lib/x86_64-\n%.0s' {1..10} >> /tmp/hundred.txt
printf '/usr/bin/duo(duo_string_new_size+0x4c) [0x56677c]\n%.0s' {1..10} >> /tmp/hundred.txt
printf 'InternalAllocateStr\n%.0s' {1..30} >> /tmp/hundred.txt

./tail_counter.py 80 '-- MARK --' /tmp/hundred.txt

Above says report on the search string MARK by tailing the final 80 lines of text from stdin or file

-- MARK --

Expected return numbers for above call to this Python script would be
10 1 20 20 20 20 80 30 70 4 80 0 20 50 50 80
Explaining the above:
 10 is the line number of the last occurrence
  1 is the line number of the first occurrence
 10 is the line number of the last occurrence of target where target is in a block of lines of two or more
 10 is the number of consecutive lines in which target is found for the block of lines described above
 10 is the line number of the largest block occurrence of target where target is in a block of lines of two or more
 10 is the number of consecutive lines in which target is found for the block of lines described above
 80 is the total lines processed
 10 is the total lines that contain the given target
 70 is the total lines that DO NOT contain the target
  4 is the number of UNIQUE lines that do not contain the target and are not obviously stack trace
 80 is the number of lines that are printable
  0 is the number of lines that contain 1 or more non-printable characters
 20 is the number of lines that begin with / and appear to be output from stack trace
 50 is the number of lines that do not begin with / but appear to be output from stack trace
 20 is the last occurrence that begins with / and appear to be output from stack trace
 80 is the last occurrence that do not begin with / but appear to be output from stack trace

Prefer fileinput to stdin.readlines()

OrderedDict() is available in Python 2.7 so where backward compatibility
is important, we instead choose to use a list_of_lists and dict_of_lines
remembering that dictionary order is arbitrary when not using OrderedDict()

printf '10 1 10 10 10 10 80 10 70 4 80 0 20 50 50 80' | egrep '([0-9]*[ ]+){15}[0-9]'
printf '10 1 10 10 10 10 80 10 70 4 80 0 20 50 50 80\n' | egrep '([0-9]*[ ]+){15}[0-9]'

python ./python26older/tail26counter.py 150 '-- MARK --' input/tail160input.txt
printf '10 1 10 10 10 10 20 150 20 130 2 150 0 20 50 60 90' | egrep '([0-9]*[ ]+){15}[0-9]'

"""

from __future__ import print_function

import fileinput
import re
from os import path,sep
from sys import argv,stdin
from string import printable
prog = argv[0].strip()
prog_basename = path.basename(prog)
prog_basename_extensionless = path.splitext(prog_basename)[0]

RE_LIB_X86_64 = re.compile('/lib/x86_64-')
RE_STACK_LT_SIGNAL = re.compile('<signal ')
#RE_STACK_CROTCHETS = re.compile('\(.+0x.+\)\s+\[0x\w*\d+\w*\]\Z')
#RE_STACK_CROTCHETS = re.compile('\s+\[0x\w*\d+\w*\]\Z')
RE_STACK_CROTCHETS = re.compile('\(.+0x.+\)\s+\[0x\w*\d+\w*\]\Z')
RE_STACK_CROTCHETS_SLASHED = re.compile('/.*\(.+0x.+\)\s+\[0x\w*\d+\w*\]\Z')
RE_INTERNAL_ALLOCATE_STR = re.compile('InternalAllocateStr')

SET_PRINTABLE = set(printable)

list_of_lists = []
dict_of_lines = {}
LINELIMIT = 500
NUM_OF_INTEGERS = 16
tuple_of_integers = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)


def list_of_lists_generate(lines,linelimit=None,verbosity=0):
	""" Helper to take a list of two element lists and populate
	a dict of lines from that. Useful for debugging and testing.
	"""
	if linelimit is None or linelimit == 0:
		linelimit = LINELIMIT

	if len(lines) <= linelimit:
		lines_tail = lines
	else:
		lines_tail = lines[-linelimit:]
		
	list_of_lists = []

	lines_printable = 0

	for idx,line in enumerate(lines_tail):
		if idx >= linelimit:
			break
		list_of_lists.append([idx,line])
		#dict_of_lines[idx] = line
		if set(line).issubset(SET_PRINTABLE):
			lines_printable += 1
		elif verbosity > 1:
			print("non-printable chars in line {0} given as {1}".format(idx,line))

	return list_of_lists


def dict_of_lines_from_list(list_of_lists_given):
	""" Helper to take a list of two element lists and populate
	a dict of lines from that. Useful for debugging and testing.
	"""
	dict_of_lines = {}
	for list2 in list_of_lists_given:
		idx = list2[0]
		dict_of_lines[idx] = list2[1]
	return dict_of_lines


def consecutives_dict_report(consecutives_dict):
	""" This function is really a debug helper and will probably not
	be called in normal use.
	"""
	for linenum in sorted(consecutives_dict):
		print(linenum,consecutives_dict[linenum])
	return


def process_lines(lines,linelimit=None,verbosity=0):
	""" Do initial processing on the lines given as parameter.
	"""

	global list_of_lists
	global dict_of_lines
	global tuple_of_integers

	if linelimit is None or linelimit == 0:
		linelimit = LINELIMIT

	if len(lines) <= linelimit:
		lines_tail = lines
	else:
		#lines_tail = lines[-80:]
		lines_tail = lines[-linelimit:]
		
	#print(linelimit,len(lines_tail))

	# Next we clear the global list_of_lists so that we .append() to a clean starting list
	list_of_lists = []

	lines_printable = 0

	for idx,line in enumerate(lines_tail):
		if idx >= linelimit:
			break
		list_of_lists.append([idx,line])
		dict_of_lines[idx] = line
		if set(line).issubset(SET_PRINTABLE):
			lines_printable += 1
		elif verbosity > 1:
			print("non-printable chars in line {0} given as {1}".format(idx,line))

	processed = len(list_of_lists)
	tuple_of_integers = (0,0,0,0,0,0,processed,0,0,0,lines_printable,
						 (processed-lines_printable),0,0,0,0)
	return tuple_of_integers


def process_fileinput_input(fileinput_given,linelimit=None):
	""" Populate the list named lines from fileinput and then
	hand off to process_lines()
	"""

	""" fileinput_given.filelineno() is the file number in the
	current file and fileinput_given.lineno() is the cummulative
	line number
	"""

	lines = []
	for line in fileinput_given.input(argv[3:]):
		lines.append(line)
		#print(line)

	return process_lines(lines,linelimit)


def process_detailed(target,dict_of_lines,iterable_of_integers,verbosity=0):
	""" Detailed analysis of list of lists using regular expressions and more
	In here I am using list() and tuple() to convert back and forth rather than having
	global as list_of_integers. I prefer the global NAME to include tuple to keep
	it noticably distinct.
	"""
	#global list_of_lists
	#global dict_of_lines
	if len(dict_of_lines) < 1 or target is None:
		return iterable_of_integers

	initial_lib_x86_64 = 0
	initial_signal = 0
	stack_crotchets_slashed = 0
	stack_crotchets = 0
	stack_allocate_str = 0
	nontarget_nonstack_lines = []
	nontarget_nonstack_dict = {}

	#if RE_STACK_CROTCHETS.search('/usr/bin/duo(duo_string_new_size+0x4c) [0x56677c]'):
	#	print('RE_STACK_CROTCHETS hit!')

	target_count = 0
	target_first = None
	target_last = None

	linecount = iterable_of_integers[6]
	assert iterable_of_integers[6] == len(dict_of_lines)

	consecutive_lines = []
	consecutive_count = 0
	consecutives_dict = {}
	count12last = None
	count13last = None

	for linenum in sorted(dict_of_lines):

		line = dict_of_lines[linenum]
		# Block for regular expressions for final two counts is next
		if RE_LIB_X86_64.match(line):
			initial_lib_x86_64 += 1
			if count12last is None or linenum > count12last:
				count12last = linenum
		elif RE_STACK_LT_SIGNAL.match(line):
			initial_signal += 1
			if count13last is None or linenum > count13last:
				count13last = linenum
		elif RE_STACK_CROTCHETS_SLASHED.match(line.rstrip()):
			stack_crotchets_slashed += 1
			if count12last is None or linenum > count12last:
				count12last = linenum
		elif RE_STACK_CROTCHETS.search(line):
			#print('RE_STACK_CROTCHETS so increment stack_crotchets.')
			stack_crotchets += 1
		elif RE_INTERNAL_ALLOCATE_STR.search(line):
			stack_allocate_str += 1
			if count13last is None or linenum > count13last:
				count13last = linenum
		elif not re.search(target,line):
			nontarget_nonstack_lines.append(line)
			""" Next we are keying a dictionary on full line as an alternative to set based example
			stored = set(); [ k for k in lines if k not in stored and not stored.add(k) ] 
			"""
			nontarget_nonstack_dict[line] = linenum
		else:
			pass

		if re.search(target,line):
			target_count += 1
			if target_first is None:
				target_first = linenum
			if target_last is None or linenum > target_last:
				target_last = linenum
			consecutive_count += 1
			consecutive_lines.append(line)
		else:
			# At this point we have stopped equalling target and are next line
			if len(consecutive_lines) > 1:
				consecutives_dict[linenum-1] = consecutive_lines
			consecutive_lines = []
			consecutive_count = 0

		line_stored = line

	if len(consecutive_lines) > 1:
		consecutives_dict[linenum] = consecutive_lines

	list_of_integers = list(iterable_of_integers)
	dictmaxlinelast = 0
	dictconsecutivelast = 0

	if len(consecutives_dict) > 0:
		dictmaxlinelast = max(sorted(consecutives_dict))
		dictconsecutivelast = len(consecutives_dict[dictmaxlinelast])
		if dictconsecutivelast > 0:
			dictmaxlinelast += 1
			#dictmaxlinelast += dictconsecutivelast

	""" is the line number of the last occurrence of target where target is in a block of lines of two or more
	is the number of consecutive lines in which target is found for the block of lines described above
	# The two variables before this comment block are described above. Next descriptions apply to those vars below
	is the line number of the largest block occurrence of target where target is in a block of lines of two or more
	is the number of consecutive lines in which target is found for the block of lines described above
	"""
	dictmaxlinemax = 0
	dictconsecutivemax = 0
	if target_last is not None:
		list_of_integers[0] = 1+target_last
	# For target_first and target_last we 1+ them to convert them to be ONE INDEXED
	if target_first is not None:
		list_of_integers[1] = 1+target_first
	list_of_integers[2] = dictmaxlinelast
	list_of_integers[3] = dictconsecutivelast
	if len(consecutives_dict) > 0:
		linecount_max = 0
		linenum_max = 0
		for linenum,lines in consecutives_dict.items():
			if len(lines) > linecount_max:
				linecount_max = len(lines)
				linenum_max = linenum
		if linenum_max > 0:
			dictconsecutivemax = linecount_max
			dictmaxlinemax = linenum_max
	list_of_integers[4] = 1+dictmaxlinemax
	list_of_integers[5] = dictconsecutivemax
	if verbosity > 1:
		consecutives_dict_report(consecutives_dict)
	list_of_integers[7] = target_count
	list_of_integers[8] = linecount - target_count
	list_of_integers[9] = len(nontarget_nonstack_dict)

	""" We define the next variable to aid debugging but not used ordinarily """
	nontarget_nonstack_lines_total = len(nontarget_nonstack_lines)
	assert nontarget_nonstack_lines_total >= len(nontarget_nonstack_dict)

	""" 20 is the number of lines that begin with / and appear to be output from stack trace
	50 is the number of lines that do not begin with / but appear to be output from stack trace
	"""
	list_of_integers[12] = initial_lib_x86_64 + stack_crotchets_slashed
	list_of_integers[13] = initial_signal + stack_allocate_str
	# For the next items (last counts) we 1+ them to convert them to be ONE INDEXED
	if count12last > 0:
		list_of_integers[14] = 1+count12last
	if count13last > 0:
		list_of_integers[15] = 1+count13last

	#tuple_of_integers = tuple(list_of_integers)

	return list_of_integers


if __name__ == "__main__":

	if len(argv) < 2:
		exit(130)

	target = None
	if len(argv) > 0:
		linelimit_string = argv[1]
		linelimit = int(linelimit_string)
		if len(argv) > 1:
			target = argv[2]


	exit_rc = 0
	processed_count = 0

	if len(argv) > 2:
		processed_count = process_fileinput_input(fileinput,linelimit)
		assert NUM_OF_INTEGERS == len(tuple_of_integers)
		list_of_integers = process_detailed(target,dict_of_lines,tuple_of_integers)
		assert NUM_OF_INTEGERS == len(list_of_integers)
		tuple_of_integers = tuple(list_of_integers)

	assert NUM_OF_INTEGERS == len(tuple_of_integers)
	print(chr(32).join('{0:d}'.format(num) for num in tuple_of_integers))

	exit(exit_rc)

