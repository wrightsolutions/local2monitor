#!/usr/bin/env python
# -*- coding: utf-8 -*-
from unittest import TestCase
from os import linesep
from string import ascii_lowercase,digits,printable,punctuation

import tail26counter as tc

class Printable(TestCase):

	def setUp(self):

		# chr(33) is exclamation mark (!)      ; chr(34) is double quote (")     ; chr(39) is single quote (')
		# chr(35) is hash / octothorpe             ; chr(58) is colon (:)            ; chr(61) is equals (=)
		# chr(32) is space ; chr(10) is line feed

		self.SET_COLON_SPACE_EQUALS = set([chr(58),chr(32),chr(61)])
		self.SET_PRINTABLE = set(printable)
		self.SET_PUNCTUATION = set(punctuation)
		self.SET_LOWER_PLUS_DIGITS = set(ascii_lowercase).union(digits)
		self.TARGET_MARK = '-- MARK --'
		self.STACK_DUO = '/usr/bin/duo(duo_string_new_size+0x4c) [0x56677c]'
		self.STACK_LIBX86 = '/lib/x86_64-linux-gnu/libx86.so.1'
		self.SIGNAL171 = '<signal R171 from Ehattons'
		self.STACK_ALLOCATE_STR = 'InternalAllocateStr'

		self.STUFF1 = 'Some stuff'
		self.STUFF2 = 'More stuff'
		self.STUFF3 = 'Even more stuff'

		self.FOURMIXED = 'a1\nb2\n\t\x00\nd4'
		self.FOURMIXED_SPLIT = self.FOURMIXED.split(linesep)
		self.EIGHTMIXED = 'a1\nb2\n\t\x00\nd4\n-- MARK --\n-- MARK --\nfiller\n-- MARK --'
		self.EIGHTMIXED_SPLIT = self.EIGHTMIXED.split(linesep)
		self.TWELVEMIXED = "{0}\n-- MARK --\n-- MARK --\n-- MARK --\nfiller\n{1}\n{1}\nk11\nl12".format(
			self.FOURMIXED,self.TARGET_MARK)
		self.TWELVEMIXED_SPLIT = self.TWELVEMIXED.split(linesep)
		self.TWELVE2STACK = "{0}\n-- MARK --\n-- MARK --\n-- MARK --\nfiller\n{1}\n{1}\n{2}\n{3}".format(
			self.FOURMIXED,self.TARGET_MARK,self.STACK_DUO,self.SIGNAL171)
		self.TWELVE2STACK_SPLIT = self.TWELVE2STACK.split(linesep)
		self.SIGNAL5MIDDLE = """
first
second
<signal 
fourth
fifth
"""
		self.SIGNAL5MIDDLE_SPLIT = self.SIGNAL5MIDDLE.split(linesep)
		self.DUO20RESTART = """
-- MARK --
-- MARK --
-- MARK --
-- MARK --
-- MARK --
-- MARK --
-- MARK --
/usr/bin/duo(duo_string_new_size+0x4c) [0x56677c]
filler
filler
filler
filler
filler
filler
filler
-- MARK --
-- MARK --
-- MARK --
"""
		self.DUO20RESTART_SPLIT = self.DUO20RESTART.split(linesep)

		lines65 = []
		# Define a list
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.STACK_DUO)
		lines65.append(self.STACK_DUO)
		lines65.append(self.STACK_LIBX86)
		lines65.append(self.STACK_LIBX86)
		lines65.append(self.SIGNAL171)
		lines65.append(self.STACK_ALLOCATE_STR)
		lines65.append(self.STACK_ALLOCATE_STR)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF1)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF2)
		lines65.append(self.STUFF3)
		lines65.append(self.STUFF3)
		lines65.append(self.STUFF3)
		lines65.append(self.STUFF3)
		lines65.append(self.STUFF3)
		lines65.append(self.STUFF3)
		lines65.append(self.STUFF3)
		lines65.append(self.STUFF3)
		lines65.append(self.STUFF3)
		lines65.append(self.STUFF3)
		lines65.append(self.STUFF3)
		lines65.append(self.STUFF3)
		lines65.append(self.TARGET_MARK)
		lines65.append(self.TARGET_MARK)

		self.DUO65RESTART = '\n'.join(lines65)
		self.DUO65RESTART_SPLIT = self.DUO65RESTART.split(linesep)

		lines66 = lines65[:]
		lines66.append(self.TARGET_MARK)
		self.DUO66RESTART = '\n'.join(lines66)
		self.DUO66RESTART_SPLIT = self.DUO66RESTART.split(linesep)

		"""
		Some hints about array position from when NUM_OF_INTEGERS = 14
		list_of_integers[2] = dictmaxlinelast
		list_of_integers[3] = dictconsecutivelast
		list_of_integers[4] = dictmaxlinemax
		list_of_integers[5] = dictconsecutivemax
		list_of_integers[7] = target_count
		list_of_integers[8] = linecount - target_count
		list_of_integers[9] = len(nontarget_nonstack_dict)
		list_of_integers[10] = linecount_printable
		list_of_integers[11] = linecount_nonprintable
		list_of_integers[12] = initial_lib_x86_64 + stack_crotchets_slashed
		list_of_integers[13] = initial_signal + stack_allocate_str
		"""
		return


	def test_mixed4(self):
		""" Simple four lines with mix of printable and non-printable.
		Signature for tail_counter method shown next
		process_detailed(target,dict_of_lines,iterable_of_integers)
		"""
		self.assertEqual(len(self.FOURMIXED),11)
		tuple_of_integers = tc.process_lines(self.FOURMIXED_SPLIT)
		#print(tuple_of_integers)
		linecount = tuple_of_integers[6]
		self.assertEqual(linecount,4)
		linecount_printable = tuple_of_integers[10]
		self.assertEqual(linecount_printable,3)
		linecount_nonprintable = tuple_of_integers[11]
		self.assertEqual(linecount_nonprintable,1)
		return


	def test_mixed8(self):
		""" Simple eight lines with mix of printable and non-printable.
		Does contain -- MARK -- and largest block of them is 2 lines
		list_of_integers[2] = dictmaxlinelast
		list_of_integers[3] = dictconsecutivelast
		list_of_integers[4] = dictmaxlinemax
		list_of_integers[5] = dictconsecutivemax
		list_of_integers[7] = target_count
		list_of_integers[8] = linecount - target_count
		"""
		self.assertEqual(len(self.EIGHTMIXED),51)
		tuple_of_integers = tc.process_lines(self.EIGHTMIXED_SPLIT,None,0)
		linecount = tuple_of_integers[6]
		self.assertEqual(linecount,8)
		linecount_printable = tuple_of_integers[10]
		self.assertEqual(linecount_printable,7)
		linecount_nonprintable = tuple_of_integers[11]
		self.assertEqual(linecount_nonprintable,1)
		""" Next we will need a dict_of_lines to supply to process_detailed()
		so make use of the helper method dict_of_lines_from_list()
		"""
		list_of_lists = tc.list_of_lists_generate(self.EIGHTMIXED_SPLIT)
		dict_of_lines = tc.dict_of_lines_from_list(list_of_lists)
		tuple_of_integers = tc.process_detailed(
			self.TARGET_MARK,dict_of_lines,tuple_of_integers,0)
		#print(tuple_of_integers)
		dictmaxlinelast = tuple_of_integers[2]
		dictconsecutivelast = tuple_of_integers[3]
		#print(self.EIGHTMIXED_SPLIT[4])
		#print(self.EIGHTMIXED_SPLIT[5])
		##print(self.EIGHTMIXED_SPLIT[6])
		##print(self.EIGHTMIXED_SPLIT[7])
		self.assertEqual(dictmaxlinelast,6)
		self.assertEqual(dictconsecutivelast,2)
		dictmaxlinemax = tuple_of_integers[4]
		dictconsecutivemax = tuple_of_integers[5]
		self.assertEqual(dictmaxlinemax,6)
		self.assertEqual(dictconsecutivemax,2)
		target_count = tuple_of_integers[7]
		self.assertEqual(target_count,3)
		nontarget_count = tuple_of_integers[8]
		self.assertEqual(nontarget_count,5)
		# [8, 5, 6, 2, 6, 2, 8, 3, 5, 5, 7, 1, 0, 0, 0, 0]
		return


	def test_mixed12(self):
		""" Twelve lines with mix of printable and non-printable.
		Does contain -- MARK -- and largest block of them is THREE lines
		list_of_integers[2] = dictmaxlinelast
		list_of_integers[3] = dictconsecutivelast
		list_of_integers[4] = dictmaxlinemax
		list_of_integers[5] = dictconsecutivemax
		list_of_integers[7] = target_count
		list_of_integers[8] = linecount - target_count
		"""
		self.assertEqual(len(self.TWELVEMIXED),81)
		tuple_of_integers = tc.process_lines(self.TWELVEMIXED_SPLIT,None,0)
		linecount = tuple_of_integers[6]
		self.assertEqual(linecount,12)
		linecount_printable = tuple_of_integers[10]
		self.assertEqual(linecount_printable,11)
		linecount_nonprintable = tuple_of_integers[11]
		self.assertEqual(linecount_nonprintable,1)
		""" Next we will need a dict_of_lines to supply to process_detailed()
		so make use of the helper method dict_of_lines_from_list()
		"""
		list_of_lists = tc.list_of_lists_generate(self.TWELVEMIXED_SPLIT)
		dict_of_lines = tc.dict_of_lines_from_list(list_of_lists)
		tuple_of_integers = tc.process_detailed(
			self.TARGET_MARK,dict_of_lines,tuple_of_integers,0)
		#print(tuple_of_integers)
		""" 7 ['-- MARK --', '-- MARK --', '-- MARK --']
		10 ['-- MARK --', '-- MARK --']
		"""
		dictmaxlinelast = tuple_of_integers[2]
		dictconsecutivelast = tuple_of_integers[3]
		self.assertEqual(dictmaxlinelast,10)
		self.assertEqual(dictconsecutivelast,2)
		dictmaxlinemax = tuple_of_integers[4]
		dictconsecutivemax = tuple_of_integers[5]
		self.assertEqual(dictmaxlinemax,7)
		self.assertEqual(dictconsecutivemax,3)
		target_count = tuple_of_integers[7]
		self.assertEqual(target_count,5)
		nontarget_count = tuple_of_integers[8]
		self.assertEqual(nontarget_count,7)
		nontarget_nonstack_count = tuple_of_integers[9]
		self.assertEqual(nontarget_nonstack_count,7)
		count12 = tuple_of_integers[12]
		self.assertEqual(count12,0)
		count13 = tuple_of_integers[13]
		self.assertEqual(count13,0)
		# [10, 5, 10, 2, 7, 3, 12, 5, 7, 7, 11, 1, 0, 0, 0, 0]
		return


	def test_twelve2stack(self):
		""" Twelve lines with mix of printable and non-printable and final two lines stack like
		Does contain -- MARK -- and largest block of them is THREE lines
		list_of_integers[2] = dictmaxlinelast
		list_of_integers[3] = dictconsecutivelast
		list_of_integers[4] = dictmaxlinemax
		list_of_integers[5] = dictconsecutivemax
		list_of_integers[7] = target_count
		list_of_integers[8] = linecount - target_count
		"""
		self.assertEqual(len(self.TWELVE2STACK),150)
		tuple_of_integers = tc.process_lines(self.TWELVE2STACK_SPLIT,None,0)
		linecount = tuple_of_integers[6]
		self.assertEqual(linecount,12)
		linecount_printable = tuple_of_integers[10]
		self.assertEqual(linecount_printable,11)
		linecount_nonprintable = tuple_of_integers[11]
		self.assertEqual(linecount_nonprintable,1)
		""" Next we will need a dict_of_lines to supply to process_detailed()
		so make use of the helper method dict_of_lines_from_list()
		"""
		list_of_lists = tc.list_of_lists_generate(self.TWELVE2STACK_SPLIT)
		dict_of_lines = tc.dict_of_lines_from_list(list_of_lists)
		tuple_of_integers = tc.process_detailed(
			self.TARGET_MARK,dict_of_lines,tuple_of_integers,0)
		#print(tuple_of_integers)
		""" 7 ['-- MARK --', '-- MARK --', '-- MARK --']
		10 ['-- MARK --', '-- MARK --']
		"""
		dictmaxlinelast = tuple_of_integers[2]
		dictconsecutivelast = tuple_of_integers[3]
		self.assertEqual(dictmaxlinelast,10)
		self.assertEqual(dictconsecutivelast,2)
		dictmaxlinemax = tuple_of_integers[4]
		dictconsecutivemax = tuple_of_integers[5]
		self.assertEqual(dictmaxlinemax,7)
		self.assertEqual(dictconsecutivemax,3)
		target_count = tuple_of_integers[7]
		self.assertEqual(target_count,5)
		nontarget_count = tuple_of_integers[8]
		self.assertEqual(nontarget_count,7)
		nontarget_nonstack_count = tuple_of_integers[9]
		self.assertEqual(nontarget_nonstack_count,5)
		count12 = tuple_of_integers[12]
		self.assertEqual(count12,1)
		count13 = tuple_of_integers[13]
		self.assertEqual(count13,1)
		# [10, 5, 10, 2, 7, 3, 12, 5, 7, 5, 11, 1, 1, 1, 11, 12]
		return


	def test_signal5(self):
		return


	def test_duo16restart(self):
		""" Fourteen lines with mix of printable and non-printable and final two lines stack like
		Does contain -- MARK -- and largest block of them is THREE lines
		list_of_integers[2] = dictmaxlinelast
		list_of_integers[3] = dictconsecutivelast
		list_of_integers[4] = dictmaxlinemax
		list_of_integers[5] = dictconsecutivemax
		list_of_integers[7] = target_count
		list_of_integers[8] = linecount - target_count
		"""
		self.assertEqual(len(self.DUO20RESTART),210)
		tuple_of_integers = tc.process_lines(self.DUO20RESTART_SPLIT,None,0)
		linecount = tuple_of_integers[6]
		self.assertEqual(linecount,20)
		linecount_printable = tuple_of_integers[10]
		self.assertEqual(linecount_printable,20)
		linecount_nonprintable = tuple_of_integers[11]
		self.assertEqual(linecount_nonprintable,0)
		""" Next we will need a dict_of_lines to supply to process_detailed()
		so make use of the helper method dict_of_lines_from_list()
		"""
		list_of_lists = tc.list_of_lists_generate(self.DUO20RESTART_SPLIT)
		dict_of_lines = tc.dict_of_lines_from_list(list_of_lists)
		tuple_of_integers = tc.process_detailed(
			self.TARGET_MARK,dict_of_lines,tuple_of_integers,0)
		#print(tuple_of_integers)
		""" 8 ['-- MARK --', '-- MARK --', '-- MARK --', '-- MARK --', '-- MARK --', '-- MARK --', '-- MARK --']
		19 ['-- MARK --', '-- MARK --', '-- MARK --']
		"""
		dictmaxlinelast = tuple_of_integers[2]
		dictconsecutivelast = tuple_of_integers[3]
		self.assertEqual(dictmaxlinelast,19)
		self.assertEqual(dictconsecutivelast,3)
		dictmaxlinemax = tuple_of_integers[4]
		dictconsecutivemax = tuple_of_integers[5]
		self.assertEqual(dictmaxlinemax,8)
		self.assertEqual(dictconsecutivemax,7)
		target_count = tuple_of_integers[7]
		self.assertEqual(target_count,10)
		nontarget_count = tuple_of_integers[8]
		self.assertEqual(nontarget_count,10)
		nontarget_nonstack_count = tuple_of_integers[9]
		self.assertEqual(nontarget_nonstack_count,2)
		count12 = tuple_of_integers[12]
		self.assertEqual(count12,1)
		""" count12 above will include the single line that includes /usr/bin/duo
		whereas count13 below will be zero
		"""
		count13 = tuple_of_integers[13]
		self.assertEqual(count13,0)
		count12last = tuple_of_integers[14]
		self.assertEqual(count12last,9)
		""" count12last is at sixth line where lines are counted ONE INDEXED
		whereas count13last can only be zero because count13 is zero
		"""
		count13last = tuple_of_integers[15]
		self.assertEqual(count13last,0)
		# [19, 2, 19, 3, 8, 7, 20, 10, 10, 2, 20, 0, 1, 0, 9, 0]
		return


	def test_duo65restart(self):
		""" Sixty five lines all printable and final two lines being the target
		Does contain -- MARK -- and largest block of them is TWENTY lines
		list_of_integers[2] = dictmaxlinelast
		list_of_integers[3] = dictconsecutivelast
		list_of_integers[4] = dictmaxlinemax
		list_of_integers[5] = dictconsecutivemax
		list_of_integers[7] = target_count
		list_of_integers[8] = linecount - target_count
		"""
		self.assertEqual(len(self.DUO65RESTART),932)
		tuple_of_integers = tc.process_lines(self.DUO65RESTART_SPLIT,None,0)
		linecount = tuple_of_integers[6]
		self.assertEqual(linecount,65)
		linecount_printable = tuple_of_integers[10]
		self.assertEqual(linecount_printable,65)
		linecount_nonprintable = tuple_of_integers[11]
		self.assertEqual(linecount_nonprintable,0)
		#print(self.DUO65RESTART_SPLIT[63])
		#print(self.DUO65RESTART_SPLIT[64])
		""" Next we will need a dict_of_lines to supply to process_detailed()
		so make use of the helper method dict_of_lines_from_list()
		"""
		list_of_lists = tc.list_of_lists_generate(self.DUO65RESTART_SPLIT)
		dict_of_lines = tc.dict_of_lines_from_list(list_of_lists)
		tuple_of_integers = tc.process_detailed(
			self.TARGET_MARK,dict_of_lines,tuple_of_integers,0)
		#print(tuple_of_integers)
		dictmaxlinelast = tuple_of_integers[2]
		dictconsecutivelast = tuple_of_integers[3]
		self.assertEqual(dictmaxlinelast,65)
		self.assertEqual(dictconsecutivelast,2)
		dictmaxlinemax = tuple_of_integers[4]
		dictconsecutivemax = tuple_of_integers[5]
		self.assertEqual(dictmaxlinemax,20)
		self.assertEqual(dictconsecutivemax,20)
		target_count = tuple_of_integers[7]
		self.assertEqual(target_count,22)
		nontarget_count = tuple_of_integers[8]
		self.assertEqual(nontarget_count,43)
		nontarget_nonstack_uniq = tuple_of_integers[9]
		self.assertEqual(nontarget_nonstack_uniq,3)
		count12 = tuple_of_integers[12]
		self.assertEqual(count12,4)
		""" count12 above will include the single line that includes /usr/bin/duo
		whereas count13 below will be zero
		"""
		count13 = tuple_of_integers[13]
		self.assertEqual(count13,3)
		count12last = tuple_of_integers[14]
		self.assertEqual(count12last,24)
		""" count12last is at sixth line where lines are counted ONE INDEXED
		whereas count13last can only be zero because count13 is zero
		"""
		count13last = tuple_of_integers[15]
		self.assertEqual(count13last,27)
		# [65, 1, 65, 2, 20, 20, 65, 22, 43, 3, 65, 0, 4, 3, 24, 27]
		return


	def test_duo66restart(self):
		""" Sixty six lines all printable and final two lines being the target
		Does contain -- MARK -- and largest block of them is TWENTY lines
		list_of_integers[2] = dictmaxlinelast
		list_of_integers[3] = dictconsecutivelast
		list_of_integers[4] = dictmaxlinemax
		list_of_integers[5] = dictconsecutivemax
		list_of_integers[7] = target_count
		list_of_integers[8] = linecount - target_count
		"""
		self.assertEqual(len(self.DUO66RESTART),943)
		tuple_of_integers = tc.process_lines(self.DUO66RESTART_SPLIT,None,0)
		linecount = tuple_of_integers[6]
		self.assertEqual(linecount,66)
		linecount_printable = tuple_of_integers[10]
		self.assertEqual(linecount_printable,66)
		linecount_nonprintable = tuple_of_integers[11]
		self.assertEqual(linecount_nonprintable,0)
		#print(self.DUO66RESTART_SPLIT[63])
		#print(self.DUO66RESTART_SPLIT[64])
		#print(self.DUO66RESTART_SPLIT[65])
		""" Next we will need a dict_of_lines to supply to process_detailed()
		so make use of the helper method dict_of_lines_from_list()
		"""
		list_of_lists = tc.list_of_lists_generate(self.DUO66RESTART_SPLIT)
		dict_of_lines = tc.dict_of_lines_from_list(list_of_lists)
		tuple_of_integers = tc.process_detailed(
			self.TARGET_MARK,dict_of_lines,tuple_of_integers,0)
		#print(tuple_of_integers)
		dictmaxlinelast = tuple_of_integers[2]
		dictconsecutivelast = tuple_of_integers[3]
		self.assertEqual(dictmaxlinelast,66)
		self.assertEqual(dictconsecutivelast,3)
		dictmaxlinemax = tuple_of_integers[4]
		dictconsecutivemax = tuple_of_integers[5]
		self.assertEqual(dictmaxlinemax,20)
		self.assertEqual(dictconsecutivemax,20)
		target_count = tuple_of_integers[7]
		self.assertEqual(target_count,23)
		nontarget_count = tuple_of_integers[8]
		self.assertEqual(nontarget_count,43)
		nontarget_nonstack_uniq = tuple_of_integers[9]
		self.assertEqual(nontarget_nonstack_uniq,3)
		count12 = tuple_of_integers[12]
		self.assertEqual(count12,4)
		""" count12 above will include the single line that includes /usr/bin/duo
		whereas count13 below will be zero
		"""
		count13 = tuple_of_integers[13]
		self.assertEqual(count13,3)
		count12last = tuple_of_integers[14]
		self.assertEqual(count12last,24)
		""" count12last is at sixth line where lines are counted ONE INDEXED
		whereas count13last can only be zero because count13 is zero
		"""
		count13last = tuple_of_integers[15]
		self.assertEqual(count13last,27)
		# [66, 1, 66, 3, 20, 20, 66, 23, 43, 3, 66, 0, 4, 3, 24, 27]
		return

