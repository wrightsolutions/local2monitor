#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015, Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

"""
Based on value in string of integers with length NUM_OF_INTEGERS
determine a status of OK, WARN, CRIT, UNKNOWN

Here CRIT is a detectable stack trace not followed by -- MARK --

Here we make this distinction between WARN and UNKNOWN

WARN if detectable stack trace but it is in the first half of the lines of the log
but not in the latter half

UNKNOWN if no -- MARK -- entries or stack trace followed by single -- MARK --

OK if tail of file is consecutive -- MARK --
"""

from __future__ import print_function

#import re
from os import path,sep
from sys import argv
import shlex
import subprocess

prog = argv[0].strip()
prog_basename = path.basename(prog)
prog_basename_extensionless = path.splitext(prog_basename)[0]

EXIT_OK = 0
EXIT_WARN = 1
EXIT_CRIT = 2
EXIT_UNKNOWN = 3

LINELIMIT = 500
NUM_OF_INTEGERS = 16

TARGET = '-- MARK --'
LOGPATH = '/tmp/tailable.log'
COUNTERSCRIPT = './tail26counter.py'
BUFFER_SIZE = 8192


def interpret_tail_output(target,logfile,linelimit=500,verbosity=0):

	counter_cmd = "python {0} {1} '{2}' {3}".format(
		COUNTERSCRIPT,linelimit,target,logfile)
	pcounter = subprocess.Popen(shlex.split(counter_cmd),bufsize=BUFFER_SIZE,
								stdout=subprocess.PIPE,shell=False)
	pcounter_stdout, pcounter_stderr = pcounter.communicate()
	pcounter_rc = pcounter.returncode

	if pcounter_rc > 0:
		return EXIT_UNKNOWN

	line1 = None
	multiple_lines = False
	for idx,line in enumerate(pcounter_stdout.splitlines()):
		if line1 is None:
			line1 = line
		else:
			multiple_lines = True

	if multiple_lines is True:
		return EXIT_UNKNOWN

	list_of_strings = line.split()

	try:
		list_of_integers = [int(num) for num in list_of_strings]
	except TypeError:
		return EXIT_UNKNOWN

	#
	targetlast = list_of_integers[0]
	targetfirst = list_of_integers[1]
	targetmaxlinelast = list_of_integers[2]
	targetconsecutivelast = list_of_integers[3]
	#
	targetmaxlinemax = list_of_integers[4]
	targetconsecutivemax = list_of_integers[5]
	#
	linecount = list_of_integers[6]
	target_count = list_of_integers[7]
	nontarget_count = list_of_integers[8]
	nontarget_nonstack_uniq = list_of_integers[9]
	#
	lines_printable = list_of_integers[10]
	lines_nonprintable = list_of_integers[11]
	#
	count12 = list_of_integers[12]
	count13 = list_of_integers[13]
	count12last = list_of_integers[14]
	count13last = list_of_integers[15]

	if linecount < 2:
		return EXIT_UNKNOWN

	try:
		linecount_halved = linecount//2
	except TypeError:
		return EXIT_UNKNOWN

	if count12 > 0 and count13 > 0:
		# Some detectable stack trace activity of both types

		if targetmaxlinelast == linecount:
			# Consecutive is better than just single entry
			return EXIT_OK
		elif count12last > (linecount - 1):
			return EXIT_CRIT
		elif count13last > (linecount - 1):
			return EXIT_CRIT
		elif count12last < linecount_halved and count13last < linecount_halved:
			return EXIT_WARN
		elif targetlast == linecount:
			# Single entry to be given benefit of doubt and assume OK also
			return EXIT_OK

	elif targetmaxlinelast == linecount:
		# Consecutive is better than just single entry
		return EXIT_OK

	elif targetlast == linecount:
		# Single entry to be given benefit of doubt and assume OK also
		return EXIT_OK

	elif count12 > 0:
		return EXIT_WARN

	elif count13 > 0:
		return EXIT_WARN

	else:
		pass

	return EXIT_UNKNOWN


if __name__ == "__main__":

	# if len(argv) < 2: exit(130)
	verbosity = 0
	if len(argv) > 1:
		verbosity = int(argv[1])

	exit_rc = 130

	return_num = interpret_tail_output(TARGET,LOGPATH,LINELIMIT,verbosity)
	if verbosity > 1:
		print("Returning {0:d} after interpretation.\n".format(return_num))
	if return_num in [0,1,2,3]:
		exit_rc = return_num

	exit(exit_rc)

