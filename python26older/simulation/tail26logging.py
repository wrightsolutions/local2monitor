#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015, Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

"""
Output predefined text to a log file in /tmp/ that simulates
log output to be analysed by tail_counter.py or similar
"""

from __future__ import print_function

#import re
from os import path,sep
from sys import argv
from string import printable
from time import sleep

prog = argv[0].strip()
prog_basename = path.basename(prog)
prog_basename_extensionless = path.splitext(prog_basename)[0]

TARGET_MARK = '-- MARK --'
STACK_DUO = '/usr/bin/duo(duo_string_new_size+0x4c) [0x56677c]'
STACK_LIBX86 = '/lib/x86_64-linux-gnu/libx86.so.1'
SIGNAL171 = '<signal R171 from Ehattons'
STACK_ALLOCATE_STR = 'InternalAllocateStr'
STUFF1 = 'Some stuff'
STUFF2 = 'More stuff'
STUFF3 = 'Even more stuff'

""" list_of_lists is abbreviated to lol
Each 3 element list is of form [sleepsecs,copies,text]
#list_of_lists = []
"""
lol = []
lol.append([0.5,20,TARGET_MARK])
lol.append([4,2,STACK_DUO])
lol.append([4,2,STACK_LIBX86])
lol.append([4,1,SIGNAL171])
lol.append([4,2,STACK_ALLOCATE_STR])
lol.append([5,12,STUFF1])
lol.append([5,12,STUFF2])
lol.append([5,12,STUFF3])
""" lol.append([0.5,2,TARGET_MARK])
in theory should detect consecutive but our implementation requires THREE
consecutive if the consecutives include the final line
"""
lol.append([0.5,3,TARGET_MARK])

LINELIMIT = 500
NUM_OF_INTEGERS = 16

OUTFPATH = '/tmp/tailable.log'

def list_of_lists_looper(list_of_lists):

	written_count = 0

	with open(OUTFPATH, 'wt') as outf:
		for list3 in list_of_lists:
			secs = list3[0]
			copies = list3[1]
			text = list3[2]
			outf.flush()
			for copy in xrange(1,copies+1):
				sleep(secs)
				outf.write("{0}\n".format(text))
				if 0 == copy % 5:
					outf.flush()
				written_count += 1
		# Automatically close OUTFPATH

	return written_count



if __name__ == "__main__":

	# if len(argv) < 2: exit(130)

	exit_rc = 130

	written_count = list_of_lists_looper(lol)
	if written_count > 0:
		exit_rc = 0
	print("Wrote {0:d} lines in total.\n".format(written_count))

	exit(exit_rc)

