#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2016, Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from __future__ import print_function

""" Analyze the first n lines of content from stdin and looking for the supplied start and
end strings in an attempt to estimate duration(s)
"""

"""
./tail26duration 500 2 'system is ready to accept connections' 'database system is shut down'
First argument says limit your processing to first 500 lines of what you are given
Second argument says to use the first 2 fields of each line in your calculation of duration
"""

import fileinput
import re
from calendar import timegm
from collections import namedtuple
from datetime import datetime as dt
from os import path,sep
from sys import argv,stdin
from string import printable
prog = argv[0].strip()
prog_basename = path.basename(prog)
prog_basename_extensionless = path.splitext(prog_basename)[0]

RE_LIB_X86_64 = re.compile('/lib/x86_64-')
RE_STACK_LT_SIGNAL = re.compile('<signal ')
RE_STACK_LT_INITIAL = re.compile('^\s*<')
RE_DATELIKE = re.compile('^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.]((19|20)\d\d)\Z')
#RE_STACK_CROTCHETS = re.compile('\(.+0x.+\)\s+\[0x\w*\d+\w*\]\Z')
#RE_STACK_CROTCHETS = re.compile('\s+\[0x\w*\d+\w*\]\Z')
RE_STACK_CROTCHETS = re.compile('\(.+0x.+\)\s+\[0x\w*\d+\w*\]\Z')
RE_STACK_CROTCHETS_SLASHED = re.compile('/.*\(.+0x.+\)\s+\[0x\w*\d+\w*\]\Z')
RE_INTERNAL_ALLOCATE_STR = re.compile('InternalAllocateStr')

SET_PRINTABLE = set(printable)
YYYYMD_WITH_TIME_DASHED = '%Y-%m-%d %H:%M:%S'
DMYYYY_WITH_TIME_SLASHED = '%d/%m/%Y %H:%M:%S'

NIXTIME2000 = 946684800
DURATION_DAY = 60*60*24
DURATION_DAY_MILLIS = 1000*60*60*24
DURATION_WEEK_MILLIS = 1000*7*60*60*24

list_of_lists = []
dict_of_lines = {}
list_of_pairs = []
LINELIMIT = 500
NUM_OF_INTEGERS = 20
""" line number of closing element of last pairing
line number of closing element of first pairing
number of pairings in total
total number of lines processed
total number of lines that contain the given closing element
total number of lines that do not contain either opening of closing element
duration of last pairing in milliseconds
duration of first pairing in milliseconds
number of lines that are printable
number of lines that contain 1 or more non-printable characters
duration shortest in milliseconds
duration longest in milliseconds
duration average in milliseconds
duration longest line number
number of lines that begin with / and appear to be output from stack trace
number of lines that do not begin with / but appear to be output from stack trace
last occurrence that begins with / and appear to be output from stack trace
last occurrence that do not begin with / but appear to be output from stack trace
number of lines starting with a 'date like' string
number of lines unscanned / skipped
"""

tuple_of_integers = tuple([0]*NUM_OF_INTEGERS)

TargetPair = namedtuple('TargetPair', ['startln','endln','millis',
                                       'pair_open_starttime','pair_open_endtime',
                                       'pair_open_startline','pair_open_endline'])

datetime_parser_flag = True
try:
        import dateutil
except:
        datetime_parser_flag = False
        

def date3tuple(date_unparsed):
        """ 
        If we do not have datetime module installed via apt-get install python-dateutil
        then assume the following date is a typical example and parse for that form:
        18/05/2016 00:04:52
        Parse default will be '%d/%m/%Y %H:%M:%S'
        Set date_parsed = 0 if there is any detected error during processing
        """
        nixtime = 0
        tuple3 = (0,None,date_unparsed)
        if date_unparsed is None or date_unparsed == 0:
                return tuple3
        
        if datetime_parser_flag is True:
                date_parsed = dateutil.parse(date_unparsed)
        else:
                try:
                        # Assume fixed form YYYYMD_WITH_TIME_DASHED
                        date_parsed = dt.strptime(date_unparsed,YYYYMD_WITH_TIME_DASHED)
                except ValueError:
                        # Assume fixed form DMYYYY_WITH_TIME_SLASHED
                        date_parsed = dt.strptime(date_unparsed,DMYYYY_WITH_TIME_SLASHED)
        try:
                nixtime = timegm(date_parsed.timetuple())
        except:
                nixtime = 1
                date_parsed = 0
        tuple3 = (nixtime,date_parsed,date_unparsed)
        return tuple3


def list_of_lists_generate(lines,linelimit=None,verbosity=0):
	""" Helper to take a list of two element lists and populate
	a dict of lines from that. Useful for debugging and testing.
	"""
	if linelimit is None or linelimit == 0:
		linelimit = LINELIMIT

	if len(lines) <= linelimit:
		lines_tail = lines
	else:
		lines_tail = lines[-linelimit:]
		
	list_of_lists = []

	lines_printable = 0

	for idx,line in enumerate(lines_tail):
		if idx >= linelimit:
			break
		list_of_lists.append([idx,line])
		#dict_of_lines[idx] = line
		if set(line).issubset(SET_PRINTABLE):
			lines_printable += 1
		elif verbosity > 1:
			print("non-printable chars in line {0} given as {1}".format(idx,line))

	return list_of_lists


def dict_of_lines_from_list(list_of_lists_given):
	""" Helper to take a list of two element lists and populate
	a dict of lines from that. Useful for debugging and testing.
	"""
	dict_of_lines = {}
	for list2 in list_of_lists_given:
		idx = list2[0]
		dict_of_lines[idx] = list2[1]
	return dict_of_lines


def consecutives_dict_report(consecutives_dict):
	""" This function is really a debug helper and will probably not
	be called in normal use.
	"""
	for linenum in sorted(consecutives_dict):
		print(linenum,consecutives_dict[linenum])
	return


def process_lines(lines,linelimit=None,verbosity=0):
	""" Do initial processing on the lines given as parameter.
	"""

	global list_of_lists
	global dict_of_lines
	global tuple_of_integers

	if linelimit is None or linelimit == 0:
		linelimit = LINELIMIT

	if len(lines) <= linelimit:
		lines_tail = lines
	else:
		#lines_tail = lines[-80:]
		lines_tail = lines[-linelimit:]
		
	#print(linelimit,len(lines_tail))

	# Next we clear the global list_of_lists so that we .append() to a clean starting list
	list_of_lists = []

	lines_printable = 0

	for idx,line in enumerate(lines_tail):
		if idx >= linelimit:
			break
		list_of_lists.append([idx,line])
		dict_of_lines[idx] = line
		if set(line).issubset(SET_PRINTABLE):
			lines_printable += 1
		elif verbosity > 1:
			print("non-printable chars in line {0} given as {1}".format(idx,line))

	processed = len(list_of_lists)
	tuple_of_integers = (0,0,0,processed,0,0,0,0,lines_printable,
                             (processed-lines_printable),
                             0,0,0,0,0,0,0,0,0,0)
	assert NUM_OF_INTEGERS == len(tuple_of_integers)
	return tuple_of_integers


def process_fileinputs(fileinputs,linelimit=None):
	""" Populate the list named lines from fileinput and then
	hand off to process_lines()
	"""

	""" fileinput_given.filelineno() is the file number in the
	current file and fileinput_given.lineno() is the cummulative
	line number
	"""

	lines = []
	for line in fileinputs:
		lines.append(line)
		#print(line)

	return process_lines(lines,linelimit)


def target_pair_initialise(pair_open_linenum,pair_open_end,pair_open_startline,
                           pair_open_endline,date_columns=2,verbosity=0):

        cols0 = date_columns
        try:
                pair_open_date_array = pair_open_startline.split()[:cols0]
                pair_close_date_array = pair_open_endline.split()[:cols0]
                pair_open_date = ' '.join(pair_open_date_array)
                pair_close_date = ' '.join(pair_close_date_array)
        except:
                pair_open_date = 0
                pair_close_date = 0
                
        if verbosity > 0:
                print("Pair that opens on line {0} will have duration {1} -> {2}".format(
                        pair_open_linenum,pair_open_date,pair_close_date))
        millis = DURATION_WEEK_MILLIS
        """ Above we have set millis high (a week) as a design choice to prevent
        [low] misrepresentation in situations where duration cannot be calculated
        """
        pair_open_nixtime,date_parsed,date_unparsed = date3tuple(pair_open_date)
        if pair_open_nixtime > NIXTIME2000:
                if verbosity > 1:
                        print("Pair that runs {0}->{1} ".format(pair_open_linenum,pair_open_end),end='')
                        print("will derive CLOSING nixtime from date/time {0}".format(pair_close_date))
                pair_close_nixtime,date_parsed,date_unparsed = date3tuple(pair_close_date)
                if verbosity > 1: print(date_parsed,date_unparsed)
        else:
                pair_close_nixtime = -1
        if verbosity > 0:
                print("Pair that runs on {0}->{1} will have duration {2} -> {3}".format(
                        pair_open_linenum,pair_open_end,pair_open_nixtime,pair_close_nixtime))

        if pair_close_nixtime > pair_open_nixtime:
                millis = 1000*(pair_close_nixtime - pair_open_nixtime)

        tp_initialised = TargetPair(pair_open_linenum,
                                    pair_open_end,
                                    millis,
                                    pair_open_date,
                                    pair_close_date,
                                    pair_open_startline,
                                    pair_open_endline)

        return tp_initialised


def process_detailed_pairs(pairedopen,pairedclose,dict_of_lines,
                           iterable_of_integers,date_columns=2,verbosity=0):
        """ line number of closing element of last pairing
        line number of closing element of first pairing
        number of pairings in total
        total number of lines processed
        total number of lines that contain the given closing element
        total number of lines that do not contain either opening of closing element
        duration of last pairing in milliseconds
        duration of first pairing in milliseconds
        number of lines that are printable
        number of lines that contain 1 or more non-printable characters
        duration shortest in milliseconds
        duration longest in milliseconds
        duration average in milliseconds
        duration longest line number
        number of lines that begin with / and appear to be output from stack trace
        number of lines that do not begin with / but appear to be output from stack trace
        last occurrence that begins with / and appear to be output from stack trace
        last occurrence that do not begin with / but appear to be output from stack trace
        number of lines starting with a 'date like' string
        number of lines unscanned / skipped
        """

        global list_of_pairs
	list_of_integers = list(iterable_of_integers)
	dict_of_lines_len = len(dict_of_lines)
        pairedclose_count = 0
	pair_open_flag = False
	pair_open_end = -1
        pair_open_endline = None
        pair_open_end_stored = -1
	for linenum in sorted(dict_of_lines,reverse=True):
                line = dict_of_lines.get(linenum)
		if re.search(pairedclose,line):
			pair_open_flag = True
			#pair_open_end = dict_of_lines_len-linenum
                        pair_open_end = linenum
                        if verbosity > 1:
                                print("pair_open_end={0} derived from inverting {1}".format(
                                        (dict_of_lines_len-pair_open_end),linenum))
                        pairedclose_count += 1
                        pair_open_endline = line
		elif re.search(pairedopen,line):
                        linenum_inverted = dict_of_lines_len-linenum
                        if pair_open_end_stored == pair_open_end_stored:
                                # Force above condition True as we want every value to overwrite / replace
                                if verbosity > 1:
                                        print("pair_open_end_stored={0} during ".format(pair_open_end),end='')
                                        print("processing of line={0}".format(line))
                                pair_open_end_stored = pair_open_end
                        if verbosity > 1:
                                print("pair_open_linenum={0} derived from inverting {1}".format(
                                        pair_open_end,linenum))

                        tp = target_pair_initialise((dict_of_lines_len-pair_open_end),
                                                    linenum_inverted,
                                                    line,
                                                    pair_open_endline,
                                                    date_columns,
                                                    verbosity)
			pair_open_flag = False
                        list_of_pairs.append(tp)
                        pair_open_end = 0
                        pair_open_endline = False
                        """ The values we reset the above two variables to are arbitrary """
                else:
                        pass

        list_of_integers[4] = pairedclose_count
        list_of_integers[5] = dict_of_lines_len-pairedclose_count

        len_list_of_pairs = len(list_of_pairs)
        """
        if len_list_of_pairs > 0 and pair_open_end > 0:
                list_of_integers[0] = pair_open_end
        """
        if verbosity > 1:
                print("There are {0} round tents :)".format(len_list_of_pairs))
                
        list_of_integers[2] = len_list_of_pairs
        if pair_open_end_stored > 0:
                list_of_integers[1] = pair_open_end_stored 
                
        durations_ignored = False
        duration_min = 0
        duration_max = 0
        duration_max_line = 0
        duration_cumulative = 0
        duration_average = 0
        duration_first = None
        duration_last = None
        for idx,tp in enumerate(list_of_pairs):
                if tp.millis == DURATION_WEEK_MILLIS:
                        durations_ignored = True
                        break
                if tp.millis > 0:
                        duration_last = tp.millis
                        # For above instead we could have compared idx to len_list_of_pairs
                        if duration_first is None:
                                duration_first = tp.millis
                        if duration_min == 0 or tp.millis < duration_min:
                                duration_min = tp.millis
                        if duration_max == 0 or tp.millis > duration_max:
                                duration_max = tp.millis
                                if verbosity > 1:
                                        print("duration_max_line={0} based on millis={1}".format(
                                                tp.endln,tp.millis))
                                duration_max_line = tp.endln
                        duration_cumulative += tp.millis

        if verbosity > 1:
                print("durations_ignored={0}".format(durations_ignored))
        if duration_last is not None:
                list_of_integers[6] = duration_last
                list_of_integers[7] = duration_first
                        
        # Cols 10 to 13 remain zero if we have even a single indication that durations were problematic
        if duration_cumulative > 0:
                """ We want an integer result always and so do not bother doing *1.0 or float()
                or forcing use of the new (future) division function
                """
                duration_average = duration_cumulative / len(list_of_pairs)
                if verbosity > 1:
                        print("Duration (min) reported as {0} millis".format(duration_min))
                        print("Duration (max) reported as {0} millis".format(duration_max))
                        print("Duration (average) reported as {0} millis".format(duration_average))

        list_of_integers[10] = duration_min
        list_of_integers[11] = duration_max
        list_of_integers[12] = duration_average
        
        if duration_average > 0:
                # We 1+ so that we present line numbers in more human natural form 
                list_of_integers[13] = 1+duration_max_line
                
        return list_of_integers


def process_detailed(target,dict_of_lines,iterable_of_integers,
                     initial_unscanned=False,verbosity=0):
	""" Detailed analysis of list of lists using regular expressions and more
	In here I am using list() and tuple() to convert back and forth rather than having
	global as list_of_integers.
	"""
	#global list_of_lists
	#global dict_of_lines
	if len(dict_of_lines) < 1 or target is None:
		return iterable_of_integers

        count_unscanned = 0
        count_datelike = 0
	initial_lib_x86_64 = 0
	initial_signal = 0
	stack_crotchets_slashed = 0
	stack_crotchets = 0
	stack_allocate_str = 0
	nontarget_nonstack_lines = []
	nontarget_nonstack_dict = {}

	#if RE_STACK_CROTCHETS.search('/usr/bin/duo(duo_string_new_size+0x4c) [0x56677c]'):
	#	print('RE_STACK_CROTCHETS hit!')

	target_count = 0
	target_first = None
	target_last = None

	linecount = iterable_of_integers[3]
	assert iterable_of_integers[3] == len(dict_of_lines)

	consecutive_lines = []
	consecutive_count = 0
	consecutives_dict = {}
	count14last = None
	count15last = None

	for linenum in sorted(dict_of_lines):

		line = dict_of_lines[linenum]
		# Block for regular expressions for final two counts is next
                if RE_STACK_LT_INITIAL.match(line):
                        if initial_unscanned is True:
                                count_unscanned += 1
                                continue
                elif RE_DATELIKE.match(line):
                        count_datelike += 1
                        
		if RE_LIB_X86_64.match(line):
			initial_lib_x86_64 += 1
			if count14last is None or linenum > count14last:
				count14last = linenum
		elif RE_STACK_LT_SIGNAL.match(line):
			initial_signal += 1
			if count15last is None or linenum > count15last:
				count15last = linenum
		elif RE_STACK_CROTCHETS_SLASHED.match(line.rstrip()):
			stack_crotchets_slashed += 1
			if count14last is None or linenum > count14last:
				count14last = linenum
		elif RE_STACK_CROTCHETS.search(line):
			#print('RE_STACK_CROTCHETS so increment stack_crotchets.')
			stack_crotchets += 1
		elif RE_INTERNAL_ALLOCATE_STR.search(line):
			stack_allocate_str += 1
			if count15last is None or linenum > count15last:
				count15last = linenum
		elif not re.search(target,line):
			nontarget_nonstack_lines.append(line)
			""" Next we are keying a dictionary on full line as an alternative to set based example
			stored = set(); [ k for k in lines if k not in stored and not stored.add(k) ] 
			"""
			nontarget_nonstack_dict[line] = linenum
		else:
			pass

		if re.search(target,line):
			target_count += 1
			if target_first is None:
				target_first = linenum
			if target_last is None or linenum > target_last:
				target_last = linenum
			consecutive_count += 1
			consecutive_lines.append(line)
		else:
			# At this point we have stopped equalling target and are next line
			if len(consecutive_lines) > 1:
				consecutives_dict[linenum-1] = consecutive_lines
			consecutive_lines = []
			consecutive_count = 0

		line_stored = line

	if len(consecutive_lines) > 1:
		consecutives_dict[linenum] = consecutive_lines

	list_of_integers = list(iterable_of_integers)
	dictmaxlinelast = 0
	dictconsecutivelast = 0

	if len(consecutives_dict) > 0:
		dictmaxlinelast = max(sorted(consecutives_dict))
		dictconsecutivelast = len(consecutives_dict[dictmaxlinelast])
		if dictconsecutivelast > 0:
			dictmaxlinelast += 1
			#dictmaxlinelast += dictconsecutivelast

        if target_last is not None:
		list_of_integers[0] = 1+target_last

	""" is the line number of the last occurrence of target where target
        is in a block of lines of two or more
	is the number of consecutive lines in which target is found for the block of lines
        described above
	# The two variables before this comment block are described above.
        # Next descriptions apply to those vars below
	is the line number of the largest block occurrence of target where
        target is in a block of lines of two or more
	is the number of consecutive lines in which target is found for the block
        of lines described above
	"""
        """
	dictmaxlinemax = 0
	dictconsecutivemax = 0
	if target_last is not None:
		list_of_integers[0] = 1+target_last
	# For target_first and target_last we 1+ them to convert them to be ONE INDEXED
	if target_first is not None:
		list_of_integers[1] = 1+target_first
	list_of_integers[2] = dictmaxlinelast
	#list_of_integers[3] = dictconsecutivelast
	if len(consecutives_dict) > 0:
		linecount_max = 0
		linenum_max = 0
		for linenum,lines in consecutives_dict.items():
			if len(lines) > linecount_max:
				linecount_max = len(lines)
				linenum_max = linenum
		if linenum_max > 0:
			dictconsecutivemax = linecount_max
			dictmaxlinemax = linenum_max
	list_of_integers[4] = 1+dictmaxlinemax
	list_of_integers[5] = dictconsecutivemax
	if verbosity > 1:
		consecutives_dict_report(consecutives_dict)
	list_of_integers[7] = target_count
	list_of_integers[8] = linecount - target_count
        """
	list_of_integers[9] = len(nontarget_nonstack_dict)

	""" We define the next variable to aid debugging but not used ordinarily """
	nontarget_nonstack_lines_total = len(nontarget_nonstack_lines)
	assert nontarget_nonstack_lines_total >= len(nontarget_nonstack_dict)

	""" 20 is the number of lines that begin with / and appear to be output from stack trace
	50 is the number of lines that do not begin with / but appear to be output from stack trace
	"""
	list_of_integers[14] = initial_lib_x86_64 + stack_crotchets_slashed
	list_of_integers[15] = initial_signal + stack_allocate_str
	# For the next items (last counts) we 1+ them to convert them to be ONE INDEXED
	if count14last > 0:
		list_of_integers[16] = 1+count14last
	if count15last > 0:
		list_of_integers[17] = 1+count15last
        list_of_integers[19] = count_unscanned

	#tuple_of_integers = tuple(list_of_integers)

	return list_of_integers


if __name__ == "__main__":

	if len(argv) < 2:
		exit(130)

	exit_rc = 0
        linelimit = 500
	pairedopen = 'starting'
	if len(argv) > 0:
		linelimit_string = argv[1]
                try:
                        linelimit = int(linelimit_string)
                except:
                        exit_rc = 131
		if len(argv) > 1:
			date_columns_string = argv[2]
                        try:
                                date_columns = int(date_columns_string)
                        except:
                                exit_rc = 132

                        if len(argv) > 2:
                                pairedopen = argv[3]

        if exit_rc > 0:
                exit(exit_rc)

	pairedclose = 'stopping'

	if len(argv) > 3:
		pairedclose = argv[4]

        exit_rc = 0
        processed_count = 0

        if len(argv) > 4:
                VERBOSITY=0
                tuple_of_integers = process_fileinputs(fileinput.input(argv[5:]),linelimit)
                processed_count = tuple_of_integers[3]
                if VERBOSITY > 0:
                        print("Fileinputs will result in {0} lines being processed in detail".format(
                                processed_count))
                
                #list_of_integers = process_detailed(target,dict_of_lines,tuple_of_integers)
                list_of_integers = process_detailed(pairedclose,dict_of_lines,tuple_of_integers,
                                                    initial_unscanned=True,verbosity=0)
                list_of_integers = process_detailed_pairs(pairedopen,pairedclose,dict_of_lines,
                                                          list_of_integers,date_columns,VERBOSITY)

                tuple_of_integers = tuple(list_of_integers)
                
	assert NUM_OF_INTEGERS == len(tuple_of_integers)
	print(chr(32).join('{0:d}'.format(num) for num in tuple_of_integers))

	exit(exit_rc)

