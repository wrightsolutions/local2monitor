#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Generate a restart script.

The generated script will run on Debian 5 or newer.

This script tcsh_restart.py requires Python 2.6 so Debian Squeeze (6.0) or newer is required
... to run THIS GENERATOR SCRIPT
"""

from sys import argv

HASHBANG='#!/usr/bin/tcsh'
PATHSTEM='/srv/bespoke'

script_template="""{0}
# Restart {2}
source {4}/bin/env.sh

set gpid = `pidof {2}`
if ( $gpid ) then
	echo "Kill {2} pid $gpid"
	kill $gpid
	sleep {1}
	echo "Starting {2}"
	{4}/bin/{2} -c {4}/conf/{3}.conf &
	set npid = `pidof {2}`
	echo "{2} Started with pid $npid"
else
	echo "Could not find pid for {2}"
endif
"""


if __name__ == "__main__":


    exit_rc = 0
    if len(argv) < 2:
        exit(130)

    pname_str = None
    sleepsecs_str = None
    sleepsecs = 5
    if len(argv) > 0:
        sleepsecs_str = argv[1]
        sleepsecs = int(sleepsecs_str)
        if len(argv) > 1:
            pname_str = argv[2]
            if len(pname_str) <= 15:
                pname = pname_str
            else:
                exit_rc = 200
                pname = pname_str[:15]

    if pname_str is None:
        exit_rc = 150
        exit(exit_rc)

    pname_lower = pname.lower()
    stext = script_template.format(HASHBANG,sleepsecs,pname,pname_lower,PATHSTEM)
    print stext

