#!/usr/bin/env python
#
#  Copyright 2015 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

from __future__ import print_function
from __future__ import with_statement

from os import path,sep
from sys import argv
#import logging
from datetime import date
from datetime import datetime as dt
import re
from sys import argv
from time import sleep

try:
    import psutil
except ImportError:
    print("Is python-psutil (Psutil) installed?")
    #print("Is python-psutil (Psutil) installed? Import error {0}".format(e))
    # apt-get install python-psutil
    # psutil-1.0.1.win-amd64-py2.7.exe
    exit(191)

"""
try:
    import sqlite3 as db3
except ImportError:
    print("Is sqlite version 3 support installed?")
    exit(201)
"""

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'
MEG_AS_FLOAT = float(2**20)
SLEEPSECS = 300

date_today = date.today()
yyyyww = "{0}{1:<2}".format(date_today.year,date_today.isocalendar()[1])
dbpath = path.expanduser('~')


def process_sample_list(sample_list):
    if 'rss' in sorton:
        # Sort on mrss (one metric of memory)
        sorted_list = sorted(sample_list, key=lambda tup: tup[6])
    else:
        # Sort on pcpu (percent cpu)
        sorted_list = sorted(sample_list, key=lambda tup: tup[2])
    return


def looped_sampling1(pattern_given,cur=None,dbpath=None):
    """ 1440 minutes in a day. 720 iterations per day if you are sleeping
    for 2 minutes. Setting a 100 day limit for sleep(60) would be 100*1440
    but setting a 100 day limit for sleep(120) would be 100*720 which is 72000
    Why 100 days? If you have a memory leak and it is not found in the first 100
    days then you probably need a more thorough investigation than can be
    supported by this tool.

    Returns true if every iteration of the while loop completed
    We consider then that the process has survived all intensive monitoring,
    and set process_survived to True.

    This version of looped_sampling designed for version 1 of psutil method calls.
    """

    iterations_complete_flag = False

    idx_upper = 100*720

    idx = 0
    while idx < idx_upper:

        sample_list = []

        for psample in psutil.process_iter():
            sample_tuple = ()
            try:
                #psample = psutil.Process(pid_given)
                sample_pid = psample.pid
                nixtime_created = psample.create_time
                isotimeutc_created = dt.isoformat(dt.utcfromtimestamp(nixtime_created))
                pcpu = psample.get_cpu_percent()
                cputimes_tuple = psample.get_cpu_times()
                cputime1 = cputimes_tuple[0]
                cputime2 = cputimes_tuple[1]
                pmem_tuple = psample.get_memory_info()
                mrss = pmem_tuple.rss / MEG_AS_FLOAT
                mvms = pmem_tuple.vms / MEG_AS_FLOAT
                """
                mshared = pmem_tuple.shared / MEG_AS_FLOAT
                mtext = pmem_tuple.text / MEG_AS_FLOAT
                mdata = pmem_tuple.data / MEG_AS_FLOAT
                """
                cmdline_list = psample.cmdline
                cmdline = ' '.join(cmdline_list)

                if pattern_given is not None:
                    if pattern_given not in cmdline:
                        continue

                cmdword1 = psample.name
                cmdword2 = psample.cmdline[0]
            except psutil.NoSuchProcess:
                """ The process is no longer available to query """
                break

            sample_tuple = (sample_pid,cmdword1,pcpu,cputime1,cputime2,isotimeutc_created,
                            mrss,mvms,cmdword2,cmdline)

            sample_list.append(sample_tuple)

        if len(sample_list) > 0:
            process_sample_list(sample_list)

        idx += 1
        """
        try:
            cur.execute("commit;")
        except:
            pass
        """
        sleep(SLEEPSECS)
        # Sleep is the last thing we do in this loop iteration
    else:
        # We completed all iterations of the loop
        iterations_complete_flag = True

    return iterations_complete_flag


if __name__ == "__main__":

    pattern_given = None
    sorton = 'pcpu'
    if len(argv) > 1:
        pattern_given = argv[1]
        if len(argv) > 2:
            sorton = argv[2]

    try:
        looped_sampling1(pattern_given)
    except:
        raise
        exit(141)

    database_path = '{0}{1:<1}proc{2}mem2__{3}.sqlite'.format(dbpath,sep,pattern_given,yyyyww)
    #cur = connect_get_cursor(database_path)


    """
    if dbcon:
        cur = dbcon.cursor()
        try:
            cur.execute("commit;")
        except:
            pass
        dbcon.close()
    """

